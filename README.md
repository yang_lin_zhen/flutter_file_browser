[Icon resources](https://iconsax.io/)

### JSON Serializable
- Run command ```flutter pub run build_runner watch```

[Internationalization](https://flutter.cn/docs/development/accessibility-and-localization/internationalization)

运行 ```flutter gen-l10n``` 命令，您将在 ```${FLUTTER_PROJECT}/.dart_tool/flutter_gen/gen_l10n``` 中看到生成的文件。

### Delopy

#### Web
- ```flutter build web --target="lib/main_prod.dart" --base-href="/web/" --web-renderer=html --pwa-strategy=none``` disable service worker
- Artifact path: ```build/web```