import 'dart:convert';

import 'package:flutter_test/flutter_test.dart';
import 'package:flutter_file_browser/model/upload_file_result.dart';
import 'package:flutter_file_browser/model/response_body.dart';

void main() {
  setUp(() {});

  tearDown(() {});

  test('getUploadFileList', () async {
    const respString = """
    {
        "code": 200,
        "reason": "",
        "msg": "",
        "data": {
            "list": [
                {
                    "name": "fileroot/temp/flutter_windows_3.0.5-stable/flutter_windows_3.0.5-stable.zip.1_90.",
                    "is_success": true,
                    "message": "Finished fileroot/temp/flutter_windows_3.0.5-stable/flutter_windows_3.0.5-stable.zip.1_90. in 6.5106ms seconds",
                    "duration": 6510600
                }
            ]
        }
    }
    """;

    final result = UploadFileResultList.fromJson(
            RespBody.fromJson(json.decode(respString)).data)
        .fileList
        .first;
    expect(result.name,
        'fileroot/temp/flutter_windows_3.0.5-stable/flutter_windows_3.0.5-stable.zip.1_90.');
  });
}
