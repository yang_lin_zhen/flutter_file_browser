// This is a basic Flutter widget test.
//
// To perform an interaction with a widget in your test, use the WidgetTester
// utility in the flutter_test package. For example, you can send tap and scroll
// gestures. You can also use WidgetTester to find child widgets in the widget
// tree, read text, and verify that the values of widget properties are correct.

import 'package:flutter_file_browser/utils/utils.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:fs_utils/fs_utils.dart' as fs_utils;

void main() {
  test('Splitting paths', () {
    const fullPath = "/a/b/c/d/efg/hijklmn";
    expect([
      "/",
      "/a",
      "/a/b",
      "/a/b/c",
      "/a/b/c/d",
      "/a/b/c/d/efg",
      "/a/b/c/d/efg/hijklmn"
    ], splitPath(fullPath));
  });

  test("fs utils", () {
    const data = "data";
    expect(data, fs_utils.getMd5(data));
  });
}
