- [X] Universal themes and i18n
- [X] Unit test
- [X] Single click to open file
- [X] Path navigation bar
- [X] Grid layout
- [X] Play video and open image
- [X] Deployment
- [ ] Multiple selection mode
- [X] Navigation tree
- [ ] Create folder
- [ ] Delete folder
- [ ] Different size layout
- [ ] Animation
- [X] Upload single file
- [ ] Upload multiple files with progress bar

### References
[How to upload large files in flutter web?](https://stackoverflow.com/questions/70079208/how-to-upload-large-files-in-flutter-web)
[如何进行Go大文件上传？](https://juejin.cn/post/6908254301978624008)
[Flutter with gRPC made easy :)](https://gonuclei.com/resources/flutter-with-grpc-made-easy)