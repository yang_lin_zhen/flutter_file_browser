import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_file_browser/main.dart';
import 'package:flutter_file_browser/model/upload_file_result.dart';
import 'package:flutter_file_browser/ui/dialog_box.dart';
import 'package:flutter_file_browser/ui/upload_file_item.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class FileUploadDialog extends DialogBox {
  const FileUploadDialog({super.key});

  @override
  DialogBoxState get boxState => _FileUploadDialogState();

  @override
  State<StatefulWidget> createState() => boxState;
}

class _FileUploadDialogState extends DialogBoxState {
  final _selectedFiles = <UploadFileWrapper>[];

  _cancelButton() => TextButton(
      onPressed: () {
        final shouldUpdate =
            _selectedFiles.any((element) => element.progress == 1.0);
        Navigator.pop(context, shouldUpdate);
      },
      child: Text(AppLocalizations.of(context)?.l10nUploadFilesCancel ?? ""));

  void _pickFiles() async {
    final result = await FilePicker.platform.pickFiles(
      dialogTitle: AppLocalizations.of(context)?.l10nSelectFiles ?? "",
      allowMultiple: true,
      withReadStream: true,
    );
    setState(() {
      _selectedFiles
        ..clear()
        ..addAll(result?.files
                .map((e) => UploadFileWrapper(
                    e, UploadFileResult(e.name, false, "", 0)))
                .toList() ??
            []);
    });
  }

  void _uploadFile() async {
    for (var wrapper in _selectedFiles) {
      fileUploadLogic.uploadFile(wrapper.file, (result, progress) {
        final index = _selectedFiles
            .indexWhere((element) => element.result.name == wrapper.file.name);
        if (index < 0) return;
        final fileToUpdate = _selectedFiles[index];
        final List<UploadFileWrapper> newList = List.from(_selectedFiles);
        newList[index] =
            UploadFileWrapper(fileToUpdate.file, fileToUpdate.result)
              ..progress = progress;
        setState(() {
          _selectedFiles
            ..clear()
            ..addAll(newList);
        });
      });
    }
    return;
  }

  @override
  List<Widget> get actions => _selectedFiles.isNotEmpty
      ? [
          TextButton(
              onPressed: () => _uploadFile(),
              child: Text(AppLocalizations.of(context)?.l10nStartUpload ?? "")),
          _cancelButton()
        ]
      : [
          _cancelButton(),
        ];

  @override
  Widget get childWidget => _selectedFiles.isEmpty
      ? Center(
          child: ElevatedButton.icon(
            icon: const Icon(
              Icons.upload_file,
            ),
            label: Text(AppLocalizations.of(context)?.l10nSelectFiles ?? ""),
            onPressed: () => _pickFiles(),
          ),
        )
      : ListView(
          children: (_selectedFiles.map((e) {
            return [
              UploadFileItem(e.file.name, e.progress),
              const Divider(
                height: 8,
                color: Colors.transparent,
              )
            ];
          })).reduce((value, element) => [...value, ...element]),
        );

  @override
  String get dialogTitle =>
      AppLocalizations.of(context)?.l10nUploadFilesTitle ?? "";
}

class UploadFileWrapper {
  PlatformFile file;
  UploadFileResult result;
  double progress = 0;
  UploadFileWrapper(this.file, this.result);
}
