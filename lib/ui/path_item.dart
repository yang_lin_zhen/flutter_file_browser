import 'package:flutter/material.dart';
import 'package:flutter_file_browser/ui/fad_button.dart';

import 'package:collection/collection.dart';
import '../utils/utils.dart';
import 'chevron_text.dart';

class PathItem extends StatefulWidget {
  final List<String> childrenPath;
  final String parentPath;
  final Function(String) onPathSelected;
  const PathItem(
      {super.key,
      required this.childrenPath,
      required this.parentPath,
      required this.onPathSelected});

  @override
  _PathItemState createState() => _PathItemState();
}

class _PathItemState extends State<PathItem> {
  Map groupExpandState = {};

  Map<String, List<String>> groupPath() =>
      widget.childrenPath.groupListsBy((e) {
        final sizes = e.split(pathSeparator);
        if (sizes.isEmpty) {
          return "";
        }
        return sizes.first;
      });

  get paddingTimes => widget.parentPath.split(pathSeparator).length - 1;

  Widget buildSubpaths() =>
      Column(mainAxisAlignment: MainAxisAlignment.start, children: [
        ...groupPath().entries.map((e) {
          final currentPath = e.key;
          final childrenPath =
              e.value.where((element) => element != currentPath).toList();
          return Column(
            children: [
              FadButton(
                  onPressed: (selected) {
                    if (selected) {
                      widget.onPathSelected([widget.parentPath, currentPath].join(pathSeparator));
                    }
                  },
                  child: Padding(
                    padding: EdgeInsets.only(
                        left: 16.0 * paddingTimes.toDouble(), right: 16.0),
                    child: ChevronText(
                        text: currentPath,
                        initState:
                            groupState(currentPath) && childrenPath.isNotEmpty,
                        onStateChanged: (state) =>
                            updateCollapse(currentPath, state)),
                  )),
              if (childrenPath.isNotEmpty)
                SubpathItem(
                    subPaths: childrenPath,
                    absPath: widget.parentPath,
                    parentPath: currentPath,
                    collapsed: groupState(currentPath),
                    onPathSelected: widget.onPathSelected)
            ],
          );
        })
      ]);

  bool groupState(String key) => groupExpandState[key] ?? true;

  void updateCollapse(String key, bool newState) {
    setState(() {
      groupExpandState[key] = newState;
    });
  }

  @override
  Widget build(BuildContext context) {
    return widget.childrenPath.isEmpty ? Container() : buildSubpaths();
  }
}

class SubpathItem extends StatefulWidget {
  final List<String> subPaths;
  final String absPath;
  final String parentPath;
  bool collapsed;
  final Function(String) onPathSelected;
  SubpathItem(
      {super.key,
      required this.subPaths,
      required this.absPath,
      required this.parentPath,
      required this.collapsed,
      required this.onPathSelected});

  @override
  _SubpathItemState createState() => _SubpathItemState();
}

class _SubpathItemState extends State<SubpathItem>
    with SingleTickerProviderStateMixin {
  late AnimationController _controller;
  late Animation<double> _animation;

  @override
  void didUpdateWidget(covariant SubpathItem oldWidget) {
    if (widget.collapsed) {
      _controller.reverse();
    } else {
      _controller.forward();
    }
    super.didUpdateWidget(oldWidget);
  }

  @override
  void initState() {
    super.initState();
    _controller = AnimationController(
        vsync: this, duration: const Duration(milliseconds: 100));
    _animation = CurvedAnimation(parent: _controller, curve: Curves.easeIn);
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SizeTransition(
        sizeFactor: Tween(begin: 1.0, end: 0.0).animate(_animation),
        child: PathItem(
          key: Key([widget.absPath, widget.parentPath].join(pathSeparator)),
          childrenPath: widget.subPaths
              .map((item) =>
                  item.replaceFirst("${widget.parentPath}$pathSeparator", ""))
              .toList(),
          parentPath: [widget.absPath, widget.parentPath].join(pathSeparator),
          onPathSelected: widget.onPathSelected,
        ));
  }
}
