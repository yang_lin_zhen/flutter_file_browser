import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_file_browser/utils/utils.dart';

class PathSelecter extends StatefulWidget {
  final String pathString;

  const PathSelecter({Key? key, required this.pathString}) : super(key: key);

  @override
  State<PathSelecter> createState() {
    return _PathSelectorState();
  }
}

class _PathSelectorState extends State<PathSelecter> {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      scrollDirection: Axis.horizontal,
      child: Row(
        children: getStringsUI(),
      ),
    );
  }

  List<Widget> getStringsUI() {
    return widget.pathString.split(pathSeparator).map((e) => Text(e)).toList();
  }
}
