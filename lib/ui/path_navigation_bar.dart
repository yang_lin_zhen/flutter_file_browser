import 'package:flutter/material.dart';
import 'package:flutter_file_browser/logic/file_list_logic.dart';
import 'package:flutter_file_browser/main.dart';
import 'package:flutter_file_browser/utils/utils.dart';
import 'package:get_it_mixin/get_it_mixin.dart';
import 'navigation_item.dart';

class PathNavigationBar extends StatefulWidget with GetItStatefulWidgetMixin {
  PathNavigationBar({
    Key? key,
  }) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _PathNavigationBarState();
  }
}

class _PathNavigationBarState extends State<PathNavigationBar>
    with GetItStateMixin {
  @override
  Widget build(BuildContext context) {
    final currentPath = watchOnly((FileListLogic logic) => logic.currentPath);
    var paths = splitPath(currentPath);

    return Container(
      color: Theme.of(context).colorScheme.tertiaryContainer,
      child: Padding(
          padding: const EdgeInsets.only(left: 8, right: 8, top: 8, bottom: 8),
          child: Row(
              children: paths.asMap().entries.map((e) {
            var index = e.key;
            var path = e.value;
            return NavigationItem(
                name: getLastDirName(path),
                onCLickAction: () {
                  fileListLogic.currentPath = path;
                },
                isLast: index == (paths.length - 1));
          }).toList())),
    );
  }

  String getLastDirName(String fullPath) {
    if (fullPath == '/') return fullPath;
    var index = fullPath.lastIndexOf('/');
    return fullPath.substring(index + 1);
  }
}
