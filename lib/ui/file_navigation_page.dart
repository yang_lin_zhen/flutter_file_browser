import 'package:flutter/cupertino.dart';
import 'package:flutter_file_browser/main.dart';
import 'package:flutter_file_browser/ui/file_navigation_container.dart';
import 'package:flutter_file_browser/ui/path_navigation_bar.dart';

class FileNavigationPage extends StatefulWidget {
  const FileNavigationPage({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _FileNavigationPageState();
}

class _FileNavigationPageState extends State<FileNavigationPage> {
  @override
  void initState() {
    super.initState();
    fileListLogic.updateFiles(fileListLogic.currentPath);
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [PathNavigationBar(), FileNavigationContainer()],
    );
  }
}
