import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class FadButton extends StatefulWidget {
  const FadButton({
    super.key,
    required this.onPressed,
    required this.child,
  });

  final Function(bool) onPressed;
  final Widget child;

  @override
  State<FadButton> createState() => _FadButtonState();
}

class _FadButtonState extends State<FadButton>
    with SingleTickerProviderStateMixin {
  bool _focused = false;
  bool _hovering = false;
  bool _on = false;
  late final Map<Type, Action<Intent>> _actionMap;
  final Map<ShortcutActivator, Intent> _shortcutMap =
      const <ShortcutActivator, Intent>{
    SingleActivator(LogicalKeyboardKey.keyX): ActivateIntent(),
  };

  @override
  void initState() {
    super.initState();
    _actionMap = <Type, Action<Intent>>{
      ActivateIntent: CallbackAction<Intent>(
        onInvoke: (Intent intent) => _toggleState(),
      ),
    };
  }

  final colorFactor = 1.0;
  Color get color {
    Color baseColor = Theme.of(context).colorScheme.secondary.withOpacity(0.8);
    if (_on) {
      baseColor = Theme.of(context).colorScheme.primary;
    }
    if (_focused) {
      baseColor =
          Theme.of(context).colorScheme.secondary.withOpacity(colorFactor);
    }
    if (_hovering) {
      baseColor =
          Theme.of(context).colorScheme.secondary.withOpacity(colorFactor);
    }
    return baseColor;
  }

  void _toggleState() {
    setState(() {
      _on = !_on;
    });
    widget.onPressed(_on);
  }

  void _handleFocusHighlight(bool value) {
    setState(() {
      _focused = value;
    });
  }

  void _handleHoveHighlight(bool value) {
    setState(() {
      _hovering = value;
    });
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: _toggleState,
      child: FocusableActionDetector(
        actions: _actionMap,
        shortcuts: _shortcutMap,
        onShowFocusHighlight: _handleFocusHighlight,
        onShowHoverHighlight: _handleHoveHighlight,
        child: Container(
          color: color,
          child: widget.child,
        ),
      ),
    );
  }
}
