import 'package:flutter/material.dart';

class HoverAnimation extends StatefulWidget {
  final Widget child;
  final bool hovering;

  /// The effects to appied to the [child], the order of list matters!!
  final List<WidgetEffect> effects;
  const HoverAnimation({
    Key? key,
    required this.hovering,
    required this.child,
    required this.effects,
  }) : super(key: key);

  @override
  State<StatefulWidget> createState() => _HoverAnimationState();
}

class _HoverAnimationState extends State<HoverAnimation>
    with SingleTickerProviderStateMixin {
  Duration duration = const Duration(milliseconds: 150);
  Duration reverseDuration = const Duration(milliseconds: 600);
  late AnimationController controller;
  late Animation<double> animation;

  @override
  void initState() {
    super.initState();
    controller = AnimationController(
        vsync: this, duration: duration, reverseDuration: reverseDuration);
    animation = CurvedAnimation(
        parent: controller,
        curve: const Cubic(0, .2, .4, 1),
        reverseCurve: const Cubic(
          1,
          .8,
          .4,
          0,
        ));
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
      animation: animation,
      builder: ((context, child) {
        if (widget.hovering) {
          controller.forward();
        } else {
          controller.reverse();
        }
        var finalChild = child ?? Container();
        if (controller.status == AnimationStatus.dismissed) {
          return finalChild;
        }
        return mergeEffects(finalChild, context, animation.value);
      }),
      child: widget.child,
    );
  }

  Widget mergeEffects(
      Widget finalWidget, BuildContext context, double animationValue) {
    return widget.effects.fold<Widget>(finalWidget, (previousValue, element) {
      return element.effectedWidget(previousValue, context, animationValue);
    });
  }
}

class ColorBorderEffect with WidgetEffect {
  @override
  Widget effectedWidget(
          Widget widget, BuildContext context, double animationValue) =>
      DecoratedBox(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(8.0 * animationValue)),
          border: Border.all(
              color: Theme.of(context)
                  .colorScheme
                  .onBackground
                  .withOpacity(animationValue * .25),
              width: 4.0,
              style: BorderStyle.solid),
        ),
        child: widget,
      );
}

class ZoomEffect with WidgetEffect {
  @override
  Widget effectedWidget(
          Widget widget, BuildContext context, double animationValue) =>
      Transform.scale(
        scale: 1 + animationValue * .07,
        child: widget,
      );
}

mixin WidgetEffect {
  Widget effectedWidget(
      Widget widget, BuildContext context, double animationValue);
}
