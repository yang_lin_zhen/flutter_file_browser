import 'package:flutter/material.dart';

class UploadFileItem extends StatelessWidget {
  final String fileName;
  final double progress;
  const UploadFileItem(this.fileName, this.progress, {Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) => ClipRRect(
      borderRadius: const BorderRadius.all(Radius.circular(5)),
      child: Stack(
        children: [
          Positioned.fill(
              child: Container(
                  color: Theme.of(context).colorScheme.primaryContainer)),
          Positioned.fill(
            child: FractionallySizedBox(
              widthFactor: progress,
              heightFactor: 1.0,
              alignment: FractionalOffset.centerLeft,
              child: Container(color: Theme.of(context).colorScheme.primary),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(4),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  fileName,
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                  textAlign: TextAlign.start,
                ),
                Text(
                  "${(progress * 100).toStringAsFixed(0)}%",
                ),
              ],
            ),
          ),
        ],
      ));
}
