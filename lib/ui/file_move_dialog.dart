import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_file_browser/main.dart';
import 'package:flutter_file_browser/ui/dialog_box.dart';
import 'package:flutter_file_browser/ui/path_item.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class FileMoveDialog extends DialogBox {
  const FileMoveDialog({super.key});

  @override
  DialogBoxState get boxState => _FileMoveDialogState();

  @override
  State<StatefulWidget> createState() => boxState;
}

class _FileMoveDialogState extends DialogBoxState {
  final _dirs = <String>[];
  String? _srcDir;
  String? _destDir;

  @override
  void initState() {
    super.initState();
    fileListLogic.getDirectories("/").then((value) => {
          setState(() {
            _dirs
              ..clear()
              ..addAll(value);
          })
        });
  }

  void _updateSrcDir(String src) => setState(() {
        _srcDir = src;
      });

  void _updateDestDir(String dest) => setState(() {
        _destDir = dest;
      });

  @override
  String get dialogTitle => AppLocalizations.of(context)!.l10nMoveFiles;

  String get sourceTitle => [AppLocalizations.of(context)!.l10nSourceDir, _srcDir ?? ""].join(": \n");
  String get destTitle => [AppLocalizations.of(context)!.l10nDestDir, _destDir ?? ""].join(": \n");

  @override
  Widget get childWidget => Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          _dirSelectorPanel(sourceTitle, _dirs, _updateSrcDir),
          const SizedBox(width: 16),
          _dirSelectorPanel(destTitle, _dirs, _updateDestDir)
        ],
      );

  @override
  List<Widget> get actions => [_submitButton(), _cancelButton()];

  _submitButton() => TextButton(
      onPressed: () async {
        await fileMoveLogic.moveFile(_srcDir ?? "", _destDir ?? "");
      },
      child: Text(AppLocalizations.of(context)?.l10nConfirm ?? ""));

  _cancelButton() => TextButton(
      onPressed: () {
        Navigator.pop(context, false);
      },
      child: Text(AppLocalizations.of(context)?.l10nUploadFilesCancel ?? ""));

  _dirSelectorPanel(String panelTitle, List<String> subpath,
          Function(String) onPathSelected) =>
      Expanded(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Text(panelTitle),
            const SizedBox(height: 8),
            PathItem(
              childrenPath: subpath,
              parentPath: "",
              onPathSelected: onPathSelected,
            )
          ],
        ),
      );
}

class DirInfo {
  final String name;
  final String abs;

  DirInfo(this.name, this.abs);
}
