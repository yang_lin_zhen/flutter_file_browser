import 'package:flutter/material.dart';
import 'package:flutter_file_browser/model/file_info.dart';
import 'package:flutter_file_browser/ui/hover_handler.dart';

class FileIcon extends StatefulWidget {
  final FileInfo info;
  final SelectionMode selectionMode;
  final Function(FileInfo)? onCLickAction;

  String get name => info.name;

  const FileIcon(
      {Key? key,
      required this.info,
      this.selectionMode = SelectionMode.single,
      this.onCLickAction})
      : super(key: key);

  @override
  State<FileIcon> createState() => _FileIconState();
}

class _FileIconState extends State<FileIcon> {
  bool _selected = false;

  void handleClick() {
    switch (widget.selectionMode) {
      case SelectionMode.multiple:
        setState(
          () {
            _selected = !_selected;
          },
        );
        break;
      case SelectionMode.single:
        widget.onCLickAction?.call(widget.info);
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => handleClick(),
      child: HoverHandler(
          hoverEffect: ZoomWithBorder(),
          child: SizedBox(
              width: 100,
              height: 100,
              child: Stack(
                children: [
                  Center(
                    child: SizedBox(
                      width: 55,
                      height: 55,
                      child: SizedBox.expand(
                        child: Icon(
                          size: 55,
                          Icons.folder,
                          color: Theme.of(context).colorScheme.primary,
                        ),
                      ),
                    ),
                  ),
                  if (_selected) ...[
                    Positioned(
                        left: 5,
                        top: 5,
                        child: Icon(
                          Icons.check_box,
                          color: Theme.of(context).colorScheme.primary,
                        ))
                  ] else
                    ...[],
                  Positioned(
                      bottom: 4,
                      left: 4,
                      right: 4,
                      child: Center(
                          child: Container(
                              padding: const EdgeInsets.all(0),
                              decoration: BoxDecoration(
                                  color: Theme.of(context)
                                      .colorScheme
                                      .onBackground
                                      .withOpacity(.35)),
                              child: Text(
                                  overflow: TextOverflow.ellipsis,
                                  textAlign: TextAlign.center,
                                  style: Theme.of(context).textTheme.labelLarge,
                                  widget.name,
                                  maxLines: 2))))
                ],
              ))),
    );
  }
}

enum SelectionMode { single, multiple }
