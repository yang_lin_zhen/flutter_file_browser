import 'package:flutter/cupertino.dart';
import 'package:flutter_file_browser/logic/file_list_logic.dart';
import 'package:flutter_file_browser/main.dart';
import 'package:flutter_file_browser/model/file_info.dart';
import 'package:flutter_file_browser/ui/file_icon.dart';
import 'package:get_it_mixin/get_it_mixin.dart';
import 'package:plugin_utils/plugin_utils.dart';

class FileNavigationContainer extends StatefulWidget
    with GetItStatefulWidgetMixin {
  FileNavigationContainer({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _FileNavigationContainerState();
}

class _FileNavigationContainerState extends State<FileNavigationContainer>
    with GetItStateMixin {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Flexible(child: showFilesInGrid(context));
  }

  Widget showFilesInGrid(BuildContext context) {
    final List<FileInfo> files =
        watchOnly((FileListLogic logic) => logic.files);
    return GridView.custom(
      gridDelegate: const SliverGridDelegateWithMaxCrossAxisExtent(
          childAspectRatio: 1.0, maxCrossAxisExtent: 120),
      childrenDelegate: SliverChildBuilderDelegate((context, position) {
        return Center(
          child: FileIcon(
              info: files[position],
              selectionMode: SelectionMode.single,
              onCLickAction: (info) {
                final newPath = info.name.toString();
                if (info.dir) {
                  fileListLogic.joinPath(newPath);
                } else {
                  PluginUtils()
                      .openLink(fileListLogic.getStaticMediaUrl(info.abs));
                }
              }),
        );
      }, childCount: files.length),
    );
  }
}
