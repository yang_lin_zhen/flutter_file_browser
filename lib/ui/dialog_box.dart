import 'package:flutter/material.dart';

showDialogBox(BuildContext context, DialogBox dialogBody,
    {Function(bool)? onCanceled}) {
  showGeneralDialog<bool>(
          context: context,
          pageBuilder: (context, animation, secondaryAnimation) => dialogBody)
      .then((value) => onCanceled?.call(value ?? false));
}

abstract class DialogBox extends StatefulWidget {
  const DialogBox({Key? key}) : super(key: key);
  DialogBoxState get boxState;

  @override
  State<StatefulWidget> createState();
}

abstract class DialogBoxState extends State<DialogBox> {
  String get dialogTitle;
  Widget get childWidget;
  List<Widget> get actions;

  Widget get defaultSizebox => SizedBox(
        width: MediaQuery.of(context).size.width * 0.8,
        height: MediaQuery.of(context).size.height * 0.5,
        child: childWidget,
      );

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text(dialogTitle),
      content: defaultSizebox,
      actions: actions,
    );
  }
}
