import 'package:flutter/material.dart';
import 'package:flutter_file_browser/ui/hover_handler.dart';

class NavigationItem extends StatefulWidget {
  final String name;
  final bool isLast;
  final void Function() onCLickAction;

  const NavigationItem(
      {Key? key,
      required this.name,
      required this.onCLickAction,
      this.isLast = false})
      : super(key: key);

  @override
  State<NavigationItem> createState() => _NavigationItemState();
}

class _NavigationItemState extends State<NavigationItem> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: widget.onCLickAction,
        child: HoverHandler(
            hoverEffect:
                HoverColor(hoverColor: Theme.of(context).colorScheme.onPrimary),
            child: Container(
              padding: const EdgeInsets.fromLTRB(4, 2, 2, 4),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Text(
                    widget.name,
                  ),
                  if (!widget.isLast)
                    Icon(
                      size: 18,
                      Icons.chevron_right,
                      color: Theme.of(context).colorScheme.onBackground,
                    )
                ],
              ),
            )));
  }
}
