import 'package:flutter/material.dart';

import 'animate/animate.dart';

class HoverHandler extends StatefulWidget {
  final Widget child;
  final HoverEffect hoverEffect;
  const HoverHandler({Key? key, required this.child, required this.hoverEffect})
      : super(key: key);

  @override
  State<StatefulWidget> createState() => _HoverHandlerState();
}

class _HoverHandlerState extends State<HoverHandler> {
  bool _hovering = false;

  @override
  Widget build(BuildContext context) {
    return MouseRegion(
      cursor: SystemMouseCursors.click,
      onEnter: (event) => _mouseEnter(true),
      onExit: (event) => _mouseEnter(false),
      child: widget.hoverEffect.hoverWidget(_hovering, widget.child, context),
    );
  }

  void _mouseEnter(bool hover) {
    setState(() {
      _hovering = hover;
    });
  }
}

mixin HoverEffect {
  Widget hoverWidget(bool hovering, Widget widget, BuildContext context);
}

class ZoomWithBorder with HoverEffect {
  @override
  Widget hoverWidget(bool hovering, Widget widget, BuildContext context) {
    return HoverAnimation(
        effects: [ColorBorderEffect(), ZoomEffect()],
        hovering: hovering,
        child: widget);
  }
}

class HoverColor implements HoverEffect {
  final Color hoverColor;
  HoverColor({required this.hoverColor});

  @override
  Widget hoverWidget(bool hovering, Widget widget, BuildContext context) {
    if (hovering) {
      return Stack(
        children: [
          Positioned.fill(
              child: Opacity(
                  opacity: .5,
                  child: Container(
                    decoration: BoxDecoration(
                      borderRadius:
                          const BorderRadius.all(Radius.circular(4.0)),
                      color: hoverColor,
                    ),
                  ))),
          widget,
        ],
      );
    }
    return widget;
  }
}
