import 'package:flutter/material.dart';

class ChevronText extends StatefulWidget {
  final Function(bool)? onStateChanged;
  final bool initState;
  final String text;

  const ChevronText(
      {super.key,
      required this.initState,
      required this.text,
      this.onStateChanged});

  @override
  _ChevronTextState createState() => _ChevronTextState();
}

class _ChevronTextState extends State<ChevronText>
    with SingleTickerProviderStateMixin {
  late AnimationController _controller;
  late bool state;
  void _updateState(bool newState) {
    setState(() {
      state = newState;
    });
    if (state) {
      _controller.forward();
    } else {
      _controller.reverse();
    }

    widget.onStateChanged?.call(state);
  }

  @override
  void initState() {
    super.initState();
    setState(() {
      state = widget.initState;
    });
    _controller = AnimationController(
      duration: const Duration(milliseconds: 100),
      vsync: this,
    );
    if (state) {
      _controller.forward();
    } else {
      _controller.reverse();
    }
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        RotationTransition(
          turns: Tween(begin: 0.0, end: 0.25).animate(
            CurvedAnimation(
              parent: _controller,
              curve: Curves.easeInOut,
            ),
          ),
          child: GestureDetector(
            onTap: () => _updateState(!state),
            child: Container(
                padding: const EdgeInsets.all(8.0),
                child: const Icon(Icons.chevron_right)),
          ),
        ),
        const SizedBox(width: 8),
        Text(
          widget.text,
          maxLines: 1,
          overflow: TextOverflow.ellipsis,
        ),
      ],
    );
  }
}
