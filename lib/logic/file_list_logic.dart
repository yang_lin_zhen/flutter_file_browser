import 'package:flutter/cupertino.dart';
import 'package:flutter_file_browser/logic/file_logic_api_service.dart';
import 'package:flutter_file_browser/main.dart';
import 'package:flutter_file_browser/model/file_info.dart';
import 'package:get_it/get_it.dart';

class FileListLogic extends ChangeNotifier {
  String _currentPath = "/";
  String get currentPath => _currentPath;
  set currentPath(String newPath) {
    _currentPath = newPath;
    notifyListeners();
    updateFiles(currentPath);
  }

  List<FileInfo> _files = List.empty();
  List<FileInfo> get files => _files;
  set files(List<FileInfo> files) {
    _files = files;
    notifyListeners();
  }

  FileLogicAPIService get service => GetIt.I.get<FileLogicAPIService>();

  Future<void> updateFiles(String path) async {
    final newFiles = await service.getFiles(path);
    files = newFiles;
  }

  Future<List<String>> getDirectories(String path) async {
    return await service.getDirectories(path);
  }

  void joinPath(String newPath) {
    String filePath =
        [currentPath == '/' ? '' : currentPath, newPath].join('/');
    currentPath = filePath;
  }

  String getStaticMediaUrl(String absPath) {
    Uri uri = appConfig.getUri("/static/$absPath");
    String mediaUrl = uri.toString();
    return mediaUrl;
  }
}
