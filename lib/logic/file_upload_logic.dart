import 'package:file_picker/file_picker.dart';
import 'package:flutter_file_browser/logic/file_logic_api_service.dart';
import 'package:get_it/get_it.dart';

import '../model/upload_file_result.dart';

class FileUploadLogic {
  FileLogicAPIService get service => GetIt.I.get<FileLogicAPIService>();

  Future<void> uploadFile(
      PlatformFile file, Function(UploadFileResult, double) onProgress) async {
    final fileSize = file.size;
    if (service.isSmallFile(fileSize)) {
      await service.uploadSmallFile(file, onProgress);
    } else {
      await service.uploadLargeFile(file, fileSize, onProgress);
    }
  }
}
