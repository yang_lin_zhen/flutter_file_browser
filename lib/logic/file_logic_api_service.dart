import 'dart:io';

import 'package:dio/dio.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter_file_browser/model/file_info.dart';
import 'package:flutter_file_browser/model/response_body.dart';
import 'package:flutter_file_browser/repository/api_provider.dart';

import '../model/upload_file_result.dart';

class FileLogicAPIService {
  final ApiProvider _apiProvider = ApiProvider();
  FileLogicAPIService();

  bool isSmallFile(int fileSize) => fileSize <= _apiProvider.chunkSize;

  Future<List<FileInfo>> getFiles(String path) async {
    final response = await _apiProvider.dio
        .getUri(_apiProvider.uri.resolve("${_apiProvider.baseUrl}/dir$path"));
    if (response.statusCode == 200) {
      RespBody respBody = RespBody.fromJson(response.data);
      List<FileInfo> files = FileInfoList.fromJson(respBody.data).fileList;
      return files;
    } else {
      throw Exception("Error getting files ${_apiProvider.uri.path}");
    }
  }

  Future<List<String>> getDirectories(String path) async {
    final response = await _apiProvider.dio.request(
        _apiProvider.uri.resolve("${_apiProvider.baseUrl}/dir$path").toString(),
        queryParameters: {
          "dir_only": true,
        });
    if (response.statusCode == 200) {
      RespBody respBody = RespBody.fromJson(response.data);
      final json = respBody.data;
      final dirs =
          (json['list'] as List?)?.map((item) => item as String).toList() ?? [];
      return dirs;
    } else {
      throw Exception("Error getting files ${_apiProvider.uri.path}");
    }
  }

  Future<void> uploadSmallFile(
      PlatformFile file, Function(UploadFileResult, double) onProgress) async {
    // final totalBytes = await file.readStream?.toList().then(
    //     (value) => value.reduce((value, element) => [...value, ...element]));
    //cannot use the method above, will cause UI freeze
    List<int> totalBytes = [];
    await for (List<int> bytes in file.readStream!) {
      totalBytes.addAll(bytes);
    }
    final response =
        await postChunk(totalBytes, file.name, 1, 1, isSmallFile: true);
    if (response.statusCode == HttpStatus.ok) {
      final result = getUploadFileList(response.data).fileList.first;
      onProgress.call(result, 1.0);
    }
  }

  Future<void> uploadLargeFile(PlatformFile file, int fileSize,
      Function(UploadFileResult, double) onProgress) async {
    String fileName = file.name;
    final total = (fileSize / _apiProvider.chunkSize).ceil();
    var transferredBytes = 0;
    int index = 1;
    await for (List<int> bytes in file.readStream!) {
      final response = await postChunk(bytes, fileName, index, total);
      if (response.statusCode == HttpStatus.ok) {
        transferredBytes += bytes.length;
        final result = getUploadFileList(response.data).fileList.first;
        onProgress.call(
            result, transferredBytes.toDouble() / fileSize.toDouble());
      }
      index++;
      continue;
    }
    final chunkResponse = await _apiProvider.dio.postUri(
        _apiProvider.uri.resolve("${_apiProvider.baseUrl}/filechunk"),
        data: FormData.fromMap({'filename': fileName}));
    if (chunkResponse.statusCode == HttpStatus.ok) {
      final result =
          UploadFileResult.fromJson(RespBody.fromJson(chunkResponse.data).data);
      onProgress.call(result, 1.0);
    }
    return;
  }

  Future<Response> postChunk(
      List<int> chunk, String fileName, int index, int total,
      {bool isSmallFile = false}) async {
    final multipartFile = MultipartFile.fromBytes(chunk, filename: fileName);
    final formData = FormData.fromMap({
      'files': [multipartFile],
      'index': index,
      'total': total,
      'small': isSmallFile,
    });
    final response = await _apiProvider.dio.postUri(
        _apiProvider.uri.resolve("${_apiProvider.baseUrl}/fileupload"),
        data: formData);
    return response;
  }

  UploadFileResultList getUploadFileList(dynamic data) =>
      UploadFileResultList.fromJson(RespBody.fromJson(data).data);
}
