import 'package:dio/dio.dart';
import 'package:flutter_file_browser/model/move_file_result.dart';

import '../model/response_body.dart';
import '../repository/api_provider.dart';

class FileMoveLogicAPIService {
  final ApiProvider _apiProvider = ApiProvider();
  FileMoveLogicAPIService();

  Future<MoveFileResult> moveDirs(String src, String dest) async {
    final response = await _apiProvider.dio
        .postUri(_apiProvider.uri.resolve("${_apiProvider.baseUrl}/move"),
            // options: Options(
            //   headers: {
            //     "Content-Type": "application/x-www-form-urlencoded",
            //   },
            // ),
            data: {
          "src": src,
          "dest": dest,
        });
    if (response.statusCode == 200) {
      RespBody respBody = RespBody.fromJson(response.data);
      final result = MoveFileResult.fromJson(respBody.data);
      print(result);
      return result;
    } else {
      throw Exception("Error move dirs ${_apiProvider.uri.path}");
    }
  }
}
