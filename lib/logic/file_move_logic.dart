import 'package:flutter_file_browser/logic/file_move_logic_api_service.dart';
import 'package:get_it/get_it.dart';

class FileMoveLogic {
  FileMoveLogicAPIService get service => GetIt.I.get<FileMoveLogicAPIService>();
  Future<void> moveFile(String sourcePath, String destinationPath) async {
    await service.moveDirs(sourcePath, destinationPath);
  }
}
