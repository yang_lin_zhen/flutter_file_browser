import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_file_browser/logic/file_list_logic.dart';
import 'package:flutter_file_browser/logic/file_logic_api_service.dart';
import 'package:flutter_file_browser/logic/file_move_logic.dart';
import 'package:flutter_file_browser/logic/file_move_logic_api_service.dart';
import 'package:flutter_file_browser/logic/file_upload_logic.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:get_it/get_it.dart';

import 'app_config.dart';
import 'file_browser_home.dart';

void main(List<String> args) async {
  WidgetsFlutterBinding.ensureInitialized();
  String env = (args.isNotEmpty) ? args[0] : "dev";

  final configUrl = await rootBundle
      .loadString(
        'assets/config/$env.json',
      )
      .then((value) => jsonDecode(value))
      .then((value) => value['apiUrl']);
  final url = configUrl.isEmpty ? Uri.base : Uri.parse(configUrl);
  registerSingletons(url);
  runApp(const FileBrowserApp());
}

class FileBrowserApp extends StatelessWidget {
  const FileBrowserApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: "Title",
        localeResolutionCallback: (
          locale,
          supportedLocales,
        ) {
          return locale;
        },
        localizationsDelegates: const [
          AppLocalizations.delegate,
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
          GlobalCupertinoLocalizations.delegate,
        ],
        supportedLocales: const [Locale('en', ''), Locale('zh', '')],
        theme: ThemeData(
          colorScheme: ColorScheme.fromSeed(
              seedColor: Colors.teal,
              brightness: Brightness.light,
              secondaryContainer: Colors.grey,
              onSecondaryContainer: Colors.black),
        ),
        home: const FileBrowserHomePage());
  }
}

void registerSingletons(Uri url) {
  GetIt.I.registerLazySingleton(() => FileLogicAPIService());
  GetIt.I.registerLazySingleton(() => FileListLogic());
  GetIt.I
      .registerLazySingleton(() => AppConfig(url.scheme, url.host, url.port));
  GetIt.I.registerLazySingleton(() => FileUploadLogic());
  GetIt.I.registerLazySingleton(() => FileMoveLogicAPIService());
  GetIt.I.registerLazySingleton(() => FileMoveLogic());
}

FileListLogic get fileListLogic => GetIt.I.get<FileListLogic>();
AppConfig get appConfig => GetIt.I.get<AppConfig>();
FileUploadLogic get fileUploadLogic => GetIt.I.get<FileUploadLogic>();
FileMoveLogic get fileMoveLogic => GetIt.I.get<FileMoveLogic>();