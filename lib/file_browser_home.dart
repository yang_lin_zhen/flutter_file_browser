import 'dart:io';

import 'package:cross_file/cross_file.dart';
import 'package:dio/dio.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_file_browser/repository/api_provider.dart';
import 'package:flutter_file_browser/ui/dialog_box.dart';
import 'package:flutter_file_browser/ui/file_navigation_page.dart';
import 'package:flutter_file_browser/ui/file_move_dialog.dart';
import 'package:flutter_file_browser/ui/file_upload_dialog.dart';

import 'package:flutter_gen/gen_l10n/app_localizations.dart';

import 'main.dart';

class FileBrowserHomePage extends StatefulWidget {
  const FileBrowserHomePage({Key? key}) : super(key: key);

  @override
  State<FileBrowserHomePage> createState() => _FileBrowserHomePageState();
}

class _FileBrowserHomePageState extends State<FileBrowserHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          AppLocalizations.of(context)!.l10nFileBrowser,
          style: TextStyle(color: Theme.of(context).colorScheme.onPrimary),
        ),
        backgroundColor: Theme.of(context).colorScheme.primary,
      ),
      drawer: Drawer(
        backgroundColor: Theme.of(context).colorScheme.secondary,
      ),
      body: const FileNavigationPage(),
      backgroundColor: Theme.of(context).colorScheme.background,
      floatingActionButton: FloatingActionButton(
        onPressed: () => showDialogBox(
          context,
          const FileMoveDialog(),
          onCanceled: (shouldUpdate) {
            if (shouldUpdate) {
              fileListLogic.updateFiles(fileListLogic.currentPath);
            }
          },
        ),
        child: const Icon(Icons.add),
      ),
    );
  }

  Stream<Response> uploadFiles(List<PlatformFile> files, Uri uri) async* {
    Dio dio = Dio();
    dio.interceptors.add(LogInterceptor());
    var formData = FormData.fromMap({
      'files': files
          .map((file) => MultipartFile(
              Stream.fromFuture(
                  XFile.fromData(files.first.bytes!).readAsBytes()),
              file.size,
              filename: file.name))
          .toList()
    });
    final response =
        await dio.postUri(uri, data: formData, onSendProgress: (count, total) {
      print("count/total: ${(count / total * 100).toStringAsFixed(0)}%");
    },
            options: Options(headers: {
              HttpHeaders.contentTypeHeader: 'multipart/form-data',
            }));
    yield response;
  }

  Future<Response> uploadSingleFile(Dio dio, PlatformFile file, Uri uri) async {
    var formData = FormData.fromMap({
      'files': MultipartFile(file.readStream!, file.size, filename: file.name)
    });
    final response =
        await dio.postUri(uri, data: formData, onSendProgress: (count, total) {
      print("count/total: ${(count / total * 100).toStringAsFixed(0)}%");
    },
            options: Options(headers: {
              HttpHeaders.contentTypeHeader: 'multipart/form-data',
            }));
    return response;
  }

  void uploadFile(Uri uri, ApiProvider provider) async {
    final result = await FilePicker.platform.pickFiles(
      dialogTitle: "Pick files",
      allowMultiple: true,
      withReadStream: true,
    );

    if (result == null || result.files.isEmpty) return;
    return await provider.uploadFile(result.files.first, ((p0, p1) {}));
  }
}
