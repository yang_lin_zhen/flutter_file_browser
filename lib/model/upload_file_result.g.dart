// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'upload_file_result.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UploadFileResult _$UploadFileResultFromJson(Map<String, dynamic> json) =>
    UploadFileResult(
      json['name'] as String,
      json['is_success'] as bool,
      json['message'] as String,
      (json['duration'] as num).toDouble(),
    );

Map<String, dynamic> _$UploadFileResultToJson(UploadFileResult instance) =>
    <String, dynamic>{
      'name': instance.name,
      'is_success': instance.isSuccess,
      'message': instance.message,
      'duration': instance.duration,
    };

UploadFileResultList _$UploadFileResultListFromJson(
        Map<String, dynamic> json) =>
    UploadFileResultList(
      (json['list'] as List<dynamic>)
          .map((e) => UploadFileResult.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$UploadFileResultListToJson(
        UploadFileResultList instance) =>
    <String, dynamic>{
      'list': instance.fileList.map((e) => e.toJson()).toList(),
    };
