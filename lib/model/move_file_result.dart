import 'package:json_annotation/json_annotation.dart';
part 'move_file_result.g.dart';

@JsonSerializable()
class MoveFileResult {
  @JsonKey(name: "duration")
  double duration;
  @JsonKey(name: "message")
  String message;
  @JsonKey(name: "success_list")
  List<String> successList;
  @JsonKey(name: "failed_list")
  List<String> failedList;

  MoveFileResult(this.duration, this.message, this.successList, this.failedList);
  factory MoveFileResult.fromJson(Map<String, dynamic> json) =>
      _$MoveFileResultFromJson(json);

  Map<String, dynamic> toJson() => _$MoveFileResultToJson(this);
}