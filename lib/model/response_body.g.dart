// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'response_body.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RespBody _$RespBodyFromJson(Map<String, dynamic> json) => RespBody(
      json['code'] as int,
      json['reason'] as String,
      json['msg'] as String,
      json['data'] as Map<String, dynamic>,
    );

Map<String, dynamic> _$RespBodyToJson(RespBody instance) => <String, dynamic>{
      'code': instance.code,
      'reason': instance.reason,
      'msg': instance.message,
      'data': instance.data,
    };
