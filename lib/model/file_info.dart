import 'package:json_annotation/json_annotation.dart';

part 'file_info.g.dart';

@JsonSerializable()
class FileInfo {
  @JsonKey(name: "name")
  String name;
  @JsonKey(name: "abs")
  String abs;
  @JsonKey(name: "size")
  num size;
  @JsonKey(name: "date")
  String date;
  @JsonKey(name: "dir")
  bool dir;

  FileInfo(this.name, this.abs, this.size, this.date, this.dir);

  factory FileInfo.fromJson(Map<String, dynamic> json) =>
      _$FileInfoFromJson(json);

  Map<String, dynamic> toJson() => _$FileInfoToJson(this);
}

@JsonSerializable(explicitToJson: true)
class FileInfoList {
  @JsonKey(name: "list")
  List<FileInfo> fileList;

  FileInfoList(this.fileList);

  factory FileInfoList.fromJson(Map<String, dynamic> json) =>
      _$FileInfoListFromJson(json);

  Map<String, dynamic> toJson() => _$FileInfoListToJson(this);
}
