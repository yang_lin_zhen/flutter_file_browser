// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'move_file_result.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

MoveFileResult _$MoveFileResultFromJson(Map<String, dynamic> json) =>
    MoveFileResult(
      (json['duration'] as num).toDouble(),
      json['message'] as String,
      (json['success_list'] as List<dynamic>).map((e) => e as String).toList(),
      (json['failed_list'] as List<dynamic>).map((e) => e as String).toList(),
    );

Map<String, dynamic> _$MoveFileResultToJson(MoveFileResult instance) =>
    <String, dynamic>{
      'duration': instance.duration,
      'message': instance.message,
      'success_list': instance.successList,
      'failed_list': instance.failedList,
    };
