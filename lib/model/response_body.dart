import 'package:json_annotation/json_annotation.dart';

part 'response_body.g.dart';

@JsonSerializable(explicitToJson: true)
class RespBody {
  @JsonKey(name: "code")
  int code;
  @JsonKey(name: "reason")
  String reason;
  @JsonKey(name: "msg")
  String message;
  @JsonKey(name: "data")
  Map<String, dynamic> data;

  RespBody(this.code, this.reason, this.message, this.data);

  factory RespBody.fromJson(Map<String, dynamic> json) =>
      _$RespBodyFromJson(json);

  Map<String, dynamic> toJson() => _$RespBodyToJson(this);
}
