// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'file_info.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

FileInfo _$FileInfoFromJson(Map<String, dynamic> json) => FileInfo(
      json['name'] as String,
      json['abs'] as String,
      json['size'] as num,
      json['date'] as String,
      json['dir'] as bool,
    );

Map<String, dynamic> _$FileInfoToJson(FileInfo instance) => <String, dynamic>{
      'name': instance.name,
      'abs': instance.abs,
      'size': instance.size,
      'date': instance.date,
      'dir': instance.dir,
    };

FileInfoList _$FileInfoListFromJson(Map<String, dynamic> json) => FileInfoList(
      (json['list'] as List<dynamic>)
          .map((e) => FileInfo.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$FileInfoListToJson(FileInfoList instance) =>
    <String, dynamic>{
      'list': instance.fileList.map((e) => e.toJson()).toList(),
    };
