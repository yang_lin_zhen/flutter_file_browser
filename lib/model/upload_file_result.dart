import 'package:json_annotation/json_annotation.dart';

part 'upload_file_result.g.dart';

@JsonSerializable()
class UploadFileResult {
  @JsonKey(name: "name")
  String name;
  @JsonKey(name: "is_success")
  bool isSuccess;
  @JsonKey(name: "message")
  String message;
  @JsonKey(name: "duration")
  double duration;

  UploadFileResult(this.name, this.isSuccess, this.message, this.duration);

  factory UploadFileResult.fromJson(Map<String, dynamic> json) =>
      _$UploadFileResultFromJson(json);

  Map<String, dynamic> toJson() => _$UploadFileResultToJson(this);
}

@JsonSerializable(explicitToJson: true)
class UploadFileResultList {
  @JsonKey(name: "list")
  List<UploadFileResult> fileList;

  UploadFileResultList(this.fileList);

  factory UploadFileResultList.fromJson(Map<String, dynamic> json) =>
      _$UploadFileResultListFromJson(json);

  Map<String, dynamic> toJson() => _$UploadFileResultListToJson(this);
}
