class AppConfig {
  final String scheme;
  final String host;
  final int port;

  const AppConfig(this.scheme, this.host, this.port);

  Uri getUri(String path) => Uri(
      scheme: scheme,
      host: host.isEmpty ? Uri.base.host : host,
      port: port == 80 ? Uri.base.port : port,
      path: path);
}
