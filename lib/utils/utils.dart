import 'dart:io';

String pathSeparatorByPlatform() => Platform.isWindows ? '\\' : '/';
const String pathSeparator = '/';

List<String> splitPath(String fullPath) {
  var paths = fullPath.split(pathSeparator);
  var newPaths = paths.map((e) => [e]).reduce((value, element) {
    var head = value.join().isEmpty ? [pathSeparator] : value;
    var tail = element.join().isEmpty ? [] : element;
    return [
      ...head,
      [
        [value.last].join(),
        ...tail
      ].join(pathSeparator),
    ];
  });
  return newPaths.where((element) => element.isNotEmpty).toList();
}
