import 'dart:io';

import 'package:dio/dio.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter_file_browser/main.dart';
import 'package:flutter_file_browser/model/response_body.dart';
import 'package:flutter_file_browser/model/upload_file_result.dart';

class ApiProvider {
  late String scheme;
  late String? host;
  late int? port;

  final _baseUrl = "/api/v1";
  String get baseUrl => _baseUrl;
  late Uri uri;

  late Dio _dio;
  Dio get dio => _dio;

  static const _chunkSize = 10 << 20;
  int get chunkSize => _chunkSize;

  ApiProvider() {
    uri = appConfig.getUri("");
    _dio = Dio()..interceptors.add(LogInterceptor());
  }

  Future<void> uploadSmallFile(
      PlatformFile file, Function(UploadFileResult, double) onProgress) async {
    await for (List<int> bytes in file.readStream!) {
      final response =
          await postChunk(bytes, file.name, 1, 1, isSmallFile: true);
      if (response.statusCode == HttpStatus.ok) {
        final result = getUploadFileList(response.data).fileList.first;
        onProgress.call(result, 1.0);
      }
    }
  }

  Future<void> uploadLargeFile(PlatformFile file, int fileSize,
      Function(UploadFileResult, double) onProgress) async {
    String fileName = file.name;
    final total = (fileSize / chunkSize).ceil();
    var transferredBytes = 0;
    int index = 1;
    await for (List<int> bytes in file.readStream!) {
      final response = await postChunk(bytes, fileName, index, total);
      if (response.statusCode == HttpStatus.ok) {
        transferredBytes += bytes.length;
        final result = getUploadFileList(response.data).fileList.first;
        onProgress.call(
            result, transferredBytes.toDouble() / fileSize.toDouble());
      }
      index++;
      continue;
    }
    final chunkResponse = await dio.postUri(uri.resolve("$_baseUrl/filechunk"),
        data: FormData.fromMap({'filename': fileName}));
    if (chunkResponse.statusCode == HttpStatus.ok) {
      final result =
          UploadFileResult.fromJson(RespBody.fromJson(chunkResponse.data).data);
      onProgress.call(result, 1.0);
    }
    return;
  }

  Future<Response> postChunk(
      List<int> chunk, String fileName, int index, int total,
      {bool isSmallFile = false}) async {
    final multipartFile = MultipartFile.fromBytes(chunk, filename: fileName);
    final formData = FormData.fromMap({
      'files': [multipartFile],
      'index': index,
      'total': total,
      'small': isSmallFile,
    });
    final response =
        await dio.postUri(uri.resolve("$_baseUrl/fileupload"), data: formData);
    return response;
  }

  Future<void> uploadFile(
      PlatformFile file, Function(UploadFileResult, double) onProgress) async {
    final fileSize = file.size;
    if (fileSize <= chunkSize) {
      await uploadSmallFile(file, onProgress);
    } else {
      await uploadLargeFile(file, fileSize, onProgress);
    }
  }

  UploadFileResultList getUploadFileList(dynamic data) =>
      UploadFileResultList.fromJson(RespBody.fromJson(data).data);
}
