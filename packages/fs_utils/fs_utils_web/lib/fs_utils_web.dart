import 'dart:convert';
import 'dart:html';

import 'package:flutter_web_plugins/flutter_web_plugins.dart';
import 'package:fs_utils_platform_interface/fs_utils_platform_interface.dart';

class FsUtilsWeb extends FsUtilsPlatform {
  FsUtilsWeb() : super();

  @override
  Future<String?> generateMd5(String input) async {
    return input;
  }

  /// Registers this class as the default instance of [UrlLauncherPlatform].
  static void registerWith(Registrar registrar) {
    FsUtilsPlatform.instance = FsUtilsWeb();
  }
}
