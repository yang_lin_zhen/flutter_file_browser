import 'package:flutter/services.dart';
import 'fs_utils_platform_interface.dart';

class MethodChannelFsUtils extends FsUtilsPlatform {
  MethodChannelFsUtils() : super();

  static const MethodChannel _channel = MethodChannel('fs_utils');

  @override
  Future<String?> generateMd5(String input) async {
    return await _channel.invokeMethod('GenerateMd5', input);
  }
}
