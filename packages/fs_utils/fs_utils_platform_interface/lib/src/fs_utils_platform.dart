import 'package:plugin_platform_interface/plugin_platform_interface.dart';
import '../method_channel_fs_utils.dart';

abstract class FsUtilsPlatform extends PlatformInterface {
  FsUtilsPlatform() : super(token: _token);

  static final Object _token = Object();

  static FsUtilsPlatform get instance => _instance;

  static set instance(FsUtilsPlatform instance) {
    PlatformInterface.verifyToken(instance, _token);
    _instance = instance;
  }

  static FsUtilsPlatform _instance = MethodChannelFsUtils();

  Future<String?> generateMd5(String input) {
    throw UnimplementedError('generateMd5() has not been implemented.');
  }
}
