import 'package:fs_utils_platform_interface/fs_utils_platform_interface.dart';

Future<String?> getMd5(String data) async {
  return FsUtilsPlatform.instance.generateMd5(data);
}
