import 'package:flutter_test/flutter_test.dart';
import 'package:http/src/response.dart';
import 'package:plugin_utils/plugin_utils.dart';
import 'package:plugin_utils/plugin_utils_platform_interface.dart';
import 'package:plugin_utils/plugin_utils_method_channel.dart';
import 'package:plugin_platform_interface/plugin_platform_interface.dart';

class MockPluginUtilsPlatform
    with MockPlatformInterfaceMixin
    implements PluginUtilsPlatform {
  @override
  Future<String?> getPlatformVersion() => Future.value('42');

  @override
  Future<String?> openLink(String url) {
    // TODO: implement openLink
    throw UnimplementedError();
  }

  @override
  Future<Response> pickFiles() {
    // TODO: implement pickFiles
    throw UnimplementedError();
  }

  @override
  Future<void> showTooltip(String text) {
    // TODO: implement showTooltip
    throw UnimplementedError();
  }
}

void main() {
  final PluginUtilsPlatform initialPlatform = PluginUtilsPlatform.instance;

  test('$MethodChannelPluginUtils is the default instance', () {
    expect(initialPlatform, isInstanceOf<MethodChannelPluginUtils>());
  });

  test('getPlatformVersion', () async {
    PluginUtils pluginUtilsPlugin = PluginUtils();
    MockPluginUtilsPlatform fakePlatform = MockPluginUtilsPlatform();
    PluginUtilsPlatform.instance = fakePlatform;

    expect(await pluginUtilsPlugin.getPlatformVersion(), '42');
  });
}
