import 'package:http/http.dart';

import 'plugin_utils_platform_interface.dart';

class PluginUtils {
  Future<String?> getPlatformVersion() {
    return PluginUtilsPlatform.instance.getPlatformVersion();
  }

  Future<String?> openLink(String url) {
    return PluginUtilsPlatform.instance.openLink(url);
  }

  Future<void> showTooltip(String text) {
    return PluginUtilsPlatform.instance.showTooltip(text);
  }

  Future<Response> pickFiles() => PluginUtilsPlatform.instance.pickFiles();
}
