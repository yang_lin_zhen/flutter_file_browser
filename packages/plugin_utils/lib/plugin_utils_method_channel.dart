import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart';

import 'plugin_utils_platform_interface.dart';

/// An implementation of [PluginUtilsPlatform] that uses method channels.
class MethodChannelPluginUtils extends PluginUtilsPlatform {
  /// The method channel used to interact with the native platform.
  @visibleForTesting
  final methodChannel = const MethodChannel('plugin_utils');

  @override
  Future<String?> getPlatformVersion() async {
    final version =
        await methodChannel.invokeMethod<String>('getPlatformVersion');
    return version;
  }

  @override
  Future<String?> openLink(String url) async {
    final realUrl = await methodChannel.invokeMethod<String>('openLink', url);
    return realUrl;
  }

  @override
  Future<void> showTooltip(String text) async {
    await methodChannel.invokeMethod<String>('showTooltip', text);
  }

  @override
  Future<Response> pickFiles() async =>
      await methodChannel.invokeMethod('pickFiles');
}
