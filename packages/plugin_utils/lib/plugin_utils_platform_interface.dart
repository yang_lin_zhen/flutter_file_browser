import 'package:http/http.dart';
import 'package:plugin_platform_interface/plugin_platform_interface.dart';

import 'plugin_utils_method_channel.dart';

abstract class PluginUtilsPlatform extends PlatformInterface {
  /// Constructs a PluginUtilsPlatform.
  PluginUtilsPlatform() : super(token: _token);

  static final Object _token = Object();

  static PluginUtilsPlatform _instance = MethodChannelPluginUtils();

  /// The default instance of [PluginUtilsPlatform] to use.
  ///
  /// Defaults to [MethodChannelPluginUtils].
  static PluginUtilsPlatform get instance => _instance;

  /// Platform-specific implementations should set this with their own
  /// platform-specific class that extends [PluginUtilsPlatform] when
  /// they register themselves.
  static set instance(PluginUtilsPlatform instance) {
    PlatformInterface.verifyToken(instance, _token);
    _instance = instance;
  }

  Future<String?> getPlatformVersion() {
    throw UnimplementedError('platformVersion() has not been implemented.');
  }

  Future<String?> openLink(String url) {
    throw UnimplementedError('openLink($url) has not been implemented.');
  }

  Future<void> showTooltip(String text) {
    throw UnimplementedError('showTooltip($text) has not been implemented.');
  }

  Future<Response> pickFiles() {
    throw UnimplementedError('pickFiles has not been implemented.');
  }
}
