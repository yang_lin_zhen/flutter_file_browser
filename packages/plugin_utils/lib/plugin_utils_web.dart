// In order to *not* need this ignore, consider extracting the "web" version
// of your plugin as a separate package, instead of inlining it in the same
// package as the core of your plugin.
// ignore: avoid_web_libraries_in_flutter
import 'dart:html' as html;

import 'package:flutter_web_plugins/flutter_web_plugins.dart';
import 'package:http/http.dart' as http;
import 'package:http/http.dart';

import 'plugin_utils_platform_interface.dart';

/// A web implementation of the PluginUtilsPlatform of the PluginUtils plugin.
class PluginUtilsWeb extends PluginUtilsPlatform {
  /// Constructs a PluginUtilsWeb
  PluginUtilsWeb();

  static void registerWith(Registrar registrar) {
    PluginUtilsPlatform.instance = PluginUtilsWeb();
  }

  /// Returns a [String] containing the version of the platform.
  @override
  Future<String?> getPlatformVersion() async {
    final version = html.window.navigator.userAgent;
    return version;
  }

  @override
  Future<String?> openLink(String url) async {
    html.window.open(url, "_blank");
    return "";
  }

  @override
  Future<void> showTooltip(String text) async {}

  @override
  Future<Response> pickFiles() async {
    return http.Response("body", 200);
  }
}
