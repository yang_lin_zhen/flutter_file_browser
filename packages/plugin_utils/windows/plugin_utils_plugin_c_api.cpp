#include "include/plugin_utils/plugin_utils_plugin_c_api.h"

#include <flutter/plugin_registrar_windows.h>

#include "plugin_utils_plugin.h"

void PluginUtilsPluginCApiRegisterWithRegistrar(
    FlutterDesktopPluginRegistrarRef registrar) {
  plugin_utils::PluginUtilsPlugin::RegisterWithRegistrar(
      flutter::PluginRegistrarManager::GetInstance()
          ->GetRegistrar<flutter::PluginRegistrarWindows>(registrar));
}
