#ifndef FLUTTER_PLUGIN_PLUGIN_UTILS_PLUGIN_H_
#define FLUTTER_PLUGIN_PLUGIN_UTILS_PLUGIN_H_

#include <flutter/method_channel.h>
#include <flutter/plugin_registrar_windows.h>

#include <memory>

namespace plugin_utils {

class PluginUtilsPlugin : public flutter::Plugin {
 public:
  static void RegisterWithRegistrar(flutter::PluginRegistrarWindows *registrar);

  PluginUtilsPlugin();

  virtual ~PluginUtilsPlugin();

  // Disallow copy and assign.
  PluginUtilsPlugin(const PluginUtilsPlugin&) = delete;
  PluginUtilsPlugin& operator=(const PluginUtilsPlugin&) = delete;

 private:
  // Called when a method is called on this plugin's channel from Dart.
  void HandleMethodCall(
      const flutter::MethodCall<flutter::EncodableValue> &method_call,
      std::unique_ptr<flutter::MethodResult<flutter::EncodableValue>> result);
};

}  // namespace plugin_utils

#endif  // FLUTTER_PLUGIN_PLUGIN_UTILS_PLUGIN_H_
